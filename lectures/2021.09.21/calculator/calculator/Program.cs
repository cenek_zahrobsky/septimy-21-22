﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace calculator
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("Vitejte!");
            Console.WriteLine("kalkulacka");


            while (true)
            {

                Console.WriteLine();
                Console.WriteLine("Vyberte operaci");
                Console.WriteLine("1 - secti, 2 - odecti, 3 - vynasob, 4 - vydel, 5 - na druhou, 6 - odmocni");

                //int operation = int.Parse(Console.ReadLine());
                int operation;

                string operationString = Console.ReadLine();
                if (!int.TryParse(operationString, out operation))
                {
                    Console.WriteLine();
                    Console.WriteLine("[warning] Nevalidni operace");
                    //Console.ReadKey();
                    continue;
                    //return;
                }




                Console.WriteLine();
                Console.WriteLine("Zadej a");
                int a = int.Parse(Console.ReadLine());

                int b = 0;
                if (operation < 5)
                {
                    Console.WriteLine("Zadej b");
                    b = int.Parse(Console.ReadLine());
                } 



                //int result;
                double r;

                if (operation == 1)
                {
                    r = a + b;
                }
                else if (operation == 2)
                {
                    r = a - b;
                }
                else if (operation == 3)
                {
                    r = a * b;
                }
                else if (operation == 4)
                {
                    if (b == 0)
                    {
                        Console.WriteLine(); 
                        Console.WriteLine("[warning] Nesmite delit 0");
                        //Console.ReadKey();
                        continue;
                        //return;
                    }

                    r = Convert.ToDouble(a) / b;
                }
                else if (operation == 5)
                {
                    r = Math.Pow(a, 2);
                }
                else if (operation == 6)
                {
                    r = Math.Sqrt(a);
                }
                else
                {
                    //Console.WriteLine();
                    //Console.WriteLine("[warning] Nevalidni cislo operace");
                    //Console.ReadKey();
                    printWarning("Nevalidni cislo operace");
                    continue;
                    //return;
                }



                Console.WriteLine();
                Console.WriteLine("Vysledek je");
                Console.WriteLine(r);


                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("Chcete pokracovat? y/n");
                string con = Console.ReadLine();

                if (con != "y")
                {
                    break;
                }

                Console.WriteLine();
            }



            //Console.ReadKey();

        }

        private static void printWarning(string msg)
        {
            Console.WriteLine();
            Console.WriteLine("[warning] " + msg);
            //Console.ReadKey();
        }
    }
}
