Na praktickou část máte čas do konce hodiny.

Nematurnatům bude stačit, když udělají 2/3.


Praktickou část implementujte pomocí formulářové aplikace.

1) Vaším oblíbeným způsobem vytvořte uživatelské rozhraní a implementujte logiku. Použíjte alespoň 6 různých ovládacích prvkůch. 5b
	a) Pro výpočet SUM(k^x), k jdoucí od 0 do n. Uživatel zadá N a X. Př. n=4, x=2, sum = 1^2 + 2^2 + 3^2 + 4^2
	b) Pro převod z desítkové do binární nebo šestnáckové. Soustavu uživatel zvolí jinak než klasickým tlačítkem.
	c) Pro výpočet obsahu elipsy. Výsledek zaokrouhlete.
2) Elipsu vykreslete. 5b
3) Udělejte aplikaci designově "zajímavou". Např. zmeňte barvu pozadí některých prvků a hezky je umístěte. 5b
4) Vstup od uživatele zvalidujte. Alespoň někde. 5b
5) Pro libovolnou část aplikace použíjte OOP. 5b
6) Část uživatelského rozhraní implementujte do separátního formuláře. 5b
b) Použíjte MenuStrip. 5b



