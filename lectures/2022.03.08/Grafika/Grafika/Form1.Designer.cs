﻿namespace Grafika
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonElipsa = new System.Windows.Forms.Button();
            this.buttonDrawing = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.ButtonEnd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonElipsa
            // 
            this.buttonElipsa.Location = new System.Drawing.Point(13, 13);
            this.buttonElipsa.Name = "buttonElipsa";
            this.buttonElipsa.Size = new System.Drawing.Size(75, 23);
            this.buttonElipsa.TabIndex = 0;
            this.buttonElipsa.Text = "elipsa";
            this.buttonElipsa.UseVisualStyleBackColor = true;
            this.buttonElipsa.Click += new System.EventHandler(this.buttonElipsa_Click);
            // 
            // buttonDrawing
            // 
            this.buttonDrawing.Location = new System.Drawing.Point(13, 55);
            this.buttonDrawing.Name = "buttonDrawing";
            this.buttonDrawing.Size = new System.Drawing.Size(75, 23);
            this.buttonDrawing.TabIndex = 1;
            this.buttonDrawing.Text = "kresleni";
            this.buttonDrawing.UseVisualStyleBackColor = true;
            this.buttonDrawing.Click += new System.EventHandler(this.buttonDrawing_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(12, 136);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 2;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(103, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(685, 425);
            this.panel1.TabIndex = 3;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ButtonEnd
            // 
            this.ButtonEnd.Location = new System.Drawing.Point(13, 165);
            this.ButtonEnd.Name = "ButtonEnd";
            this.ButtonEnd.Size = new System.Drawing.Size(72, 23);
            this.ButtonEnd.TabIndex = 4;
            this.ButtonEnd.Text = "End";
            this.ButtonEnd.UseVisualStyleBackColor = true;
            this.ButtonEnd.Click += new System.EventHandler(this.ButtonEnd_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ButtonEnd);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.buttonDrawing);
            this.Controls.Add(this.buttonElipsa);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonElipsa;
        private System.Windows.Forms.Button buttonDrawing;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button ButtonEnd;
    }
}

