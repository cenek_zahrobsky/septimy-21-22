﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grafika
{
    public partial class Form1 : Form
    {
        Graphics g;

        public Form1()
        {
            InitializeComponent();

            //kdyz chceme kreslit uz do existujiciho krelsiciho platna
            //udalost paint se zavola pri vykresleni. Pro prekresleni musime zavolat refresh.
            //Kdyz si to vytahneme takhle, tak nemusime volat refresh
            g = panel1.CreateGraphics();
        }

        private void buttonElipsa_Click(object sender, EventArgs e)
        {
            Form ellipseForm = new EllipseForm();
            //ellipseForm.ShowDialog(); //jako dialogovy okno;
            ellipseForm.Show(); //;
        }

        private void buttonDrawing_Click(object sender, EventArgs e)
        {
            Form drawingForm = new DrawingForm();
            drawingForm.ShowDialog();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void ButtonEnd_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Random random = new Random();
            Shape shape = (Shape)random.Next(0, 4);

            int red = random.Next(0, 256);
            int green = random.Next(0, 256);
            int blue = random.Next(0, 256);
            Color color = Color.FromArgb(red, green, blue);

            var pen = new Pen(color, 1);

            int width = random.Next(0, 100);
            int height = random.Next(0, 100);

            int x = random.Next(0, panel1.Width - width);
            int y = random.Next(0, panel1.Height - height);

            bool fillled = random.Next(0, 2) == 0;

            switch (shape)
            {
                case Shape.Ellipse:
                    if (fillled)
                    {
                        Brush brush1 = new SolidBrush(color);
                        g.FillEllipse(brush1, x, y, width, height);
                    }
                    else
                    {
                        g.DrawEllipse(pen, x, y, width, height);
                    }
                    break;
                case Shape.Rectangle:
                    if (fillled)
                    {
                        Brush brush2 = new SolidBrush(color);
                        g.FillRectangle(brush2, x, y, width, height);
                    }
                    else
                    {
                        g.DrawRectangle(pen, x, y, width, height);
                    }
                    break;
                case Shape.Line:
                    g.DrawLine(pen, x, y, x + width, y + height);
                    break;
                default:
                    Font font = new Font("Arial", 14);
                    Brush brush = new SolidBrush(color);
                    g.DrawString("GYMVOD", font, brush, x, y);
                    break;
            }
        }

        enum Shape
        {
            Ellipse,
            Rectangle,
            Line,
            String,
        }
    }
}
