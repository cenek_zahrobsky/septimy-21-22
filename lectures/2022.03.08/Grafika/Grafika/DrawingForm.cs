﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grafika
{
    public partial class DrawingForm : Form
    {
        Color selectedColor;
        List<PointWithColor> points = new List<PointWithColor>();


        public DrawingForm()
        {
            InitializeComponent();
            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
            | BindingFlags.Instance | BindingFlags.NonPublic, null,
            panel1, new object[] { true });
        }

        private void buttonColor_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
                selectedColor = colorDialog1.Color;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            // starts with 1 !!!
            for (int i = 1; i < points.Count; i++)
            {
                //var pen = new Pen(points[i].Color);
                var pen = new Pen(points[i-1].color);
                g.DrawLine(pen, points[i - 1].point, points[i].point);
            }

            //navysit vykreslovaci pamet
            //pro panel nejde nastavit double buffer naprimo a museli bychom pouzit dedeni nebo

        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                PointWithColor pointWithColor = new PointWithColor();
                pointWithColor.point = e.Location;
                pointWithColor.color = selectedColor;

                //Point point = e.Location;
                points.Add(pointWithColor);
                panel1.Refresh();

            }
        }

        private class PointWithColor
        {
            public Point point { get; set; }
            public Color color { get; set; }
        }
    }
}
