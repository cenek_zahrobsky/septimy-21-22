A
Na vypracování teoretické části máte 30 minut.
Některé odpovědi se mohou lišit v jiných programovacích jazycích, ptám se na C# a "svět" .NET Frameworku.

a) Co je to algoritmus? (5b)

b) Co jsou to kompilované jazyky? Zamyslete se nad výhodami i nevýhodami. (5b)

c) Co je to datový typ? Uveďte co nejvíce příkladů. Ukažte, jak deklarujeme proměnnou. (5b)

d) Jaké znáte druhy podmínek? Popšite jednotlivé konstrukce a vysvětlete, co znamená operátor != . (5b)

e) Co je to string builder a v čem je lepší než samotný string? (5b)

f) Jaké máme typy konverzí? Vysvětlete rozdíl. (5b)

g) Jak se jmenuje třída na generování náhodných čísel? Jak se jmenuje metoda, která nám náhodné číslo vygeneruje? (5b)

h) Jak definujeme funkci s parametrem a jak ji zavoláme? (5b)

i) Jaké znáte řadící algoritmy? (5b)






