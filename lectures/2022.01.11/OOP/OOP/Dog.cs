﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    //class Dog : ICommunicable, ICommunicable 
    class Dog : Animal, ICommunicable
    {
        string name;

        public Dog(String name)
        {
            this.name = name;
        }

        public string Communicate()
        {
            return $"{name}: hafhaf";
        }

        public string Run()
        {
            return $"{name}: bezi";
        }
        
        //overloading
        public string Run(int speed)
        {
            return $"{name}: bezi rychlosti {speed}";
        }
        
        //override
        public string Move()
        {
            return $"Pes {name}: se pohybuje";
        }

        public string MoveFromBase()
        {
            return base.Move();
        }

    }
}
