﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faktorial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vypocty faktorialu");

            //Vyzkouset si opet zavolat funkce

            Console.WriteLine();
            Console.WriteLine("for"); 
            Console.WriteLine(FactorialFor(0)); //1
            Console.WriteLine(FactorialFor(1)); //1
            Console.WriteLine(FactorialFor(2)); //2
            Console.WriteLine(FactorialFor(3)); //6
            Console.WriteLine(FactorialFor(4)); //24

            Console.WriteLine();
            Console.WriteLine("while");
            Console.WriteLine(FactorialWhile(0));
            Console.WriteLine(FactorialWhile(1));
            Console.WriteLine(FactorialWhile(2));
            Console.WriteLine(FactorialWhile(3));
            Console.WriteLine(FactorialWhile(4));

            Console.WriteLine();
            Console.WriteLine("rekurze");
            Console.WriteLine(FactorialRec(0));
            Console.WriteLine(FactorialRec(1));
            Console.WriteLine(FactorialRec(2));
            Console.WriteLine(FactorialRec(3));
            Console.WriteLine(FactorialRec(4));



            //rekurze
            Console.ReadKey();
            Console.WriteLine();
            Console.WriteLine(FactorialFor(20));
            Console.WriteLine(FactorialRec(20));






            Console.ReadKey();
        }

        private static long FactorialRec(int x)
        {
            if (x <= 1)
                return 1;
            
            return x * FactorialRec(x - 1);
        }

        private static long FactorialFor(int x)
        {
            long fact = 1;
            for (int i = 1; i <= x; i++)
            {
                checked
                {
                    fact = fact * i;
                }
            }

            return fact;
        }

        private static long FactorialWhile(int x)
        {
            long fact = 1;
            int i = 1;
            while (i <= x)
            {
                fact = fact * i;
                i++;
            }

            return fact;
        }
    }
}
