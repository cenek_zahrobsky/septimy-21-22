﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prvocisla
{
    class Program
    {
        static void Main(string[] args)
        {

            //cykly
            //forcykly
            Console.WriteLine("Cykly");
            for (int i = 0; i < 10; i++)
            {
                Console.Write(i);
            }
            Console.WriteLine();

            //while
            int k = 1;
            while (k <= 10)
            {
                Console.Write(k++);
            }
            Console.WriteLine();
            
            //do while
            int l = 11;
            do
            {
                Console.Write(k++);
            } while (l <= 10);
            Console.WriteLine();

            //foreach
            //nejde menit hodnoty a ani nezname index
            int[] pole = new int[] { 1, 2, 3 };
            foreach (var item in pole)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();









            //PRVOCISLA
            //Test prvociselnosti
            Console.WriteLine("Prvocislo");
            Console.WriteLine("Zadejte cislo na overeni prvociselnosti");
            int x = int.Parse(Console.ReadLine());
            //x = int.MaxValue - 1;
            bool isPrvocislo = true;

            if (x <= 1)
                isPrvocislo = false;

            //for (int i = 2; i < x; i++)
            for (int i = 2; i < Math.Sqrt(x); i++)
            {
                //operator modulo, ktery vraci zbytek po deleni
                //chceme zachytit cislo, ktere deli nase x. Zbytek po deleni je 0
                if (x % i == 0)
                {
                    isPrvocislo = false;
                    Console.WriteLine("Cislo {0} je delitelne {1}", x, i);
                    break;
                }
            }

            if (isPrvocislo)
            {
                Console.WriteLine("Cislo {0} je prvocislo", x);
            } else
            {
                Console.WriteLine("Cislo {0} neni prvocislo", x);
            }

            //chci dve veci
            //1) optimalizovat - zrychlit ten algoritmus -- napoveda je 49 nebo 289 prvocislo?
            //      odmocnina a break
            //2) cislo x ziskejte pomoci funkce ReadNumber()


            Console.ReadKey();
        }
    }
}
