﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinForms2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void labelName_Click(object sender, EventArgs e)
        {
            textBoxName.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Uzivatel kliknul";


            if(textBoxName.Text == "")
            {
                MessageBox.Show("Musis zadat jmeno", "Chyba", MessageBoxButtons.OK);
                return;
            }

            DialogResult res = MessageBox.Show("Ahoj " + textBoxName.Text, "", MessageBoxButtons.YesNo);
            MessageBox.Show("Uzivatel vybral " + res, "odpoved");
            MessageBox.Show("Uzivatel vybral " + (res == DialogResult.Yes ? "ano" : "ne"), "odpoved");


            toolStripStatusLabel1.Text = "Aplikace bezi";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (var item in checkedListBox1.CheckedItems)
            {
                MessageBox.Show("Checked item: " + item.ToString());
            }

            webBrowser1.Url = new Uri("https://seznamzpravy.cz");            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
                checkedListBox1.SetItemChecked(i, true);
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            MessageBox.Show("Start " + e.Start.ToShortDateString() + " and end " + e.End.ToShortDateString());
        }

        private void zavritToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void barvaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var color = colorDialog1.ShowDialog();
            MessageBox.Show(color.ToString());
        }

        private void otevritToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string file = openFileDialog1.FileName;
                MessageBox.Show(file);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value > 95)
            {
                progressBar1.Value = 0;
            }
            else
            {
                progressBar1.Value += 5;
            }
        }
    }
}
