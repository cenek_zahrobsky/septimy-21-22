﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RomanNumbers
{
    public partial class Form1 : Form
    {
        Converter converter;

        public Form1()
        {
            InitializeComponent();

            //dictionary ukazka
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("po", "pondeli");
            dic.Add("ut", "utery");
            dic.Add("st", "streda");
            dic.Add("ct", "ctvrtek");
            dic.Add("pa", "patek");

            test.Text = dic["pa"];

            // test.Text = dic["so"]; musime osetrit
            if (dic.ContainsKey("so"))
                test.Text = dic["so"];

            dic.Remove("pa");
            //test.Text = dic["pa"];

            test.Text = "";
            foreach (KeyValuePair<string, string> kvp in dic)
            {
                test.Text += $"Key = {kvp.Key}, Value = {kvp.Value}; \t";
            }



            converter = new Converter();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //validace
            string input = textBox1.Text;
            int number;
            if (!int.TryParse(input, out number))
            {
                errorMessage.Text = "Zadejte validni cele cislo";
                return;
            }

            //convert
            string result = converter.ToRoman(number);


            label1.Text = result;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(errorMessage.Text))
                errorMessage.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string roman = textBox1.Text; 

            int result = converter.FromRoman(roman);

            label1.Text = result.ToString();
        }

        private void buttonBinary_Click(object sender, EventArgs e)
        {
            string input = textBoxBinary.Text;
            int number;
            if (!int.TryParse(input, out number))
            {
                errorMessage.Text = "Zadejte validni cele cislo";
                return;
            }

            labelBinary.Text = converter.DecimalToBinary(number);
        }

        private void buttonBinary2_Click(object sender, EventArgs e)
        {
            string input = textBoxBinary.Text;
            int number;
            if (!int.TryParse(input, out number))
            {
                errorMessage.Text = "Zadejte validni cele cislo";
                return;
            }

            labelBinary2.Text = converter.DecimalToBinaryEasy(number);
        }

        private void buttonDecimalEasy_Click(object sender, EventArgs e)
        {
            string input = textBoxBinary.Text;
          
            labelDecimalEasy.Text = converter.BinaryToDecimalEasy(input).ToString();
        }

        private void buttonDecimal_Click(object sender, EventArgs e)
        {
            string input = textBoxBinary.Text;

            labelDecimal.Text = converter.BinaryToDecimal(input).ToString();
        }
    }
}
