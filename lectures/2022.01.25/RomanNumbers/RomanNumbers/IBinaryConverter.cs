﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomanNumbers
{
    interface IBinaryConverter
    {
        //
        // Summary:
        //     Converts decimal number to binary number using Convert.ToString().
        //
        // Parameters:
        //     number:
        //        A int containing a number to convert.
        //
        // Return:
        //     A string representing the binary number.
        string DecimalToBinaryEasy(int number);

        //
        // Summary:
        //     Converts decimal number to binary number using your own algorithm.
        //
        // Parameters:
        //     number:
        //        A int containing a number to convert.
        //
        // Return:
        //     A string representing the binary number.
        string DecimalToBinary(int number);

        //
        // Summary:
        //     Converts binary numeral to decimal number using Convert.ToInt32().
        //
        // Parameters:
        //     binaryNumber:
        //        A string containing binary number to convert.
        //
        // Return:
        //     A int representing the decimal number.
        int BinaryToDecimalEasy(string binaryNumber);

        //
        // Summary:
        //     Converts binary numeral to decimal number using your own algorithm.
        //
        // Parameters:
        //     binaryNumber:
        //        A string containing binary number to convert.
        //
        // Return:
        //     A int representing the decimal number.
        int BinaryToDecimal(string binaryNumber);
    }
}
