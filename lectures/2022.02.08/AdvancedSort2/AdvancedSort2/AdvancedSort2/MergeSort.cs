﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedSort2
{
    class MergeSort
    {
        public void Sort(int[] arr, int start, int end)
        {
            if (end > start) {
                //find middle
                int middle = (start + end) / 2;

                //call sort for left half and right half
                Sort(arr, start, middle);
                Sort(arr, middle + 1, end);

                //call merge on sorted halves
                Merge(arr, start, middle, end);
            }
        }

        private void Merge(int[] arr, int start, int middle, int end)
        {
            // find sizes of the two halves
            int n1 = middle - start + 1;
            int n2 = end - middle;

            int[] tempL = new int[n1];
            int[] tempR = new int[n2];

            int i;
            int j;

            for (i = 0; i < n1; i++)
                tempL[i] = arr[start + i];
            for (j = 0; j < n2; j++)
                tempR[j] = arr[middle + 1 + j];

            i = 0; //tempL
            j = 0; //tempR

            //Merge
            int k = start;
            while(i < n1 && j < n2)
            {
                if (tempL[i] < tempR[j])
                {
                    arr[k] = tempL[i];
                    i++;
                }
                else
                {
                    arr[k] = tempR[j];
                    j++;
                }
                k++;
            }

            while(i < n1)
            {
                arr[k] = tempL[i];
                i++;
                k++;
            }

            while(j < n2)
            {
                arr[k] = tempR[j];
                j++;
                k++;
            }


        }
    }
}
