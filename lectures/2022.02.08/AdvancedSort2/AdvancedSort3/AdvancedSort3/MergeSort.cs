﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedSort3
{
    class MergeSort
    {

        public void Sort(int[] arr, int start, int end)
        {
            if(start < end)
            {
                //find middle
                int middle = (start + end) / 2;

                //sort left and right half
                Sort(arr, start, middle);
                Sort(arr, middle + 1, end);

                //Merge the sorted halves
                Merge(arr, start, middle, end);
            }
        }

        private void Merge(int[] arr, int start, int middle, int end)
        {
            //find sizes of two subarrays
            int nL = middle - start + 1;
            int nR = end - middle;

            int[] tempL = new int[nL];
            int[] tempR = new int[nR];
            int i, j;

            for (i = 0; i < nL; i++)
                tempL[i] = arr[start + i];
            for (j = 0; j < nR; j++)
                tempR[j] = arr[middle + j + 1];

            i = 0;
            j = 0;

            //merge
            int k = start;  
            while(i < nL && j < nR)
            {
                if (tempL[i] < tempR[j])
                {
                    arr[k] = tempL[i];
                    i++;
                }
                else
                {
                    arr[k] = tempR[j];
                    j++;
                }
                k++;
            }

            //copy remaining
            while(j < nR)
            {
                arr[k] = tempR[j];
                j++;
                k++;
            }
            //copy remaining
            while(i < nL)
            {
                arr[k] = tempL[i];
                i++;
                k++;
            }
        }
    }
}
