# Septimy 21-22

GYMVOD programovací seminář Septimy 2021/2022

## Učitelé

**David Hájek**

- hajekdd@gmail.com
- FEL
- Java, php , python, javascript


### TEST
23.11.2021 teorie + programování
28.01.2022 teorie

### Online
meet.google.com/nxh-bvgb-axg



### Soutěž Kasiopea
- https://kasiopea.matfyz.cz/


### Pravidla

- Všichni se naučíme pracovat s GITem. Materiály budou na GitLabu.
- Discord https://discord.gg/n2c2rhBKyr
- Vždycky si to z něho na začátku hodiny stáhneme.
- Úkoly budou každý týden nebo ob týden. Na vypracování máte týden,
s dobrou výmluvou máte dva týdny. Jinak dostanete úkol navíc. Za
úkoly se budou dostávat jedničky. Neopisovat, poznám to. Odevzdávat
na svůj repositář.
- Budete mít projekt na semestr ve dvojicích. Témata si představíme
příští hodinu.
- Písemky budou asi dvě - část teoretická a část praktická.
- Při hodině budete spolupracovat.

### Harmonogram 

| Datum | Učitel | Téma | Přednášky | Úkol |
| --- | --- | --- | --- | --- |
| 7.9.2021 | David | Úvod; seznámení; stránka; úkoly; semestrálka; postup při řešení problému; vlastnosti algoritmu, základní struktury; basic úloha | [lec01](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.09.07/uvodni-prezentace.pdf) | |
| 14.9.2021 | David | GIT, založit repo, nasdílet, ssh klíč; pull, add ., commit, push, rebase, merge | [lec02](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.09.14/prezentace02.pdf) | |
| 21.9.2021 | David |GIT, typy programovacích jazyků | [lec03](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.09.21/prezentace03.pdf) | [hw01](https://gitlab.com/hajekdd/septimy-21-22/-/tree/main/homework/hw01) |
| 28.9.2021 |  | svátek |  |  |
| 5.10.2021 | David |datové typy hodnotové a referenční; předdefinované datové typy; převody mezi datovými typy a jejich využití; deklarace proměnných | [lec04](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.10.05/prezentace04.pdf) | [hw02](https://gitlab.com/hajekdd/septimy-21-22/-/tree/main/homework/hw02) |
| 12.10.2021 | David | základní struktury programovacího jazyka | [lec05](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.10.12/prezentace05.pdf) | |
| 19.10.2021 | David | Cykly a Rekurze | [lec06](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.10.19/prezentace06.pdf) | [hw03](https://gitlab.com/hajekdd/septimy-21-22/-/tree/main/homework/hw03)  |
| 26.10.2021 | |  |  |  |
| 02.11.2021 | David | Rekurze 2 a seznamy | [lec07](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.11.02/prezentace07.pdf) | [hw04](https://gitlab.com/hajekdd/septimy-21-22/-/tree/main/homework/hw04)  |
| 09.11.2021 | David | Práce s textem + Práce se souborem | [lec08](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.11.09/prezentace08.pdf) [lec08.2](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.11.09/prezentace09.pdf) | [hw05](https://gitlab.com/hajekdd/septimy-21-22/-/tree/main/homework/hw05)  |
| 16.11.2021 | David | Algoritmy třídění a řazení | [lec10](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.11.16/prezentace10.pdf) | [hw06](https://gitlab.com/hajekdd/septimy-21-22/-/tree/main/homework/hw06)  |
| 23.11.2021 | David | Test| | |
| 30.11.2021 | David | Gui | [lec11](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.11.30/prezentace11.pdf) |  |
| 7.12.2021 | David | Paint a Timer | [lec12](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.12.07/prezentace12.pdf) |  |
| 14.12.2021 | David | OOP 1 | [lec13](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.12.14/prezentace13b.pdf) |  |
| 21.12.2021 | David | OOP - snowstorm |  |  |
| 11.01.2022 | David | OOP 2 | [lec14+17](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.01.11/prezentace14.pdf) | HW - test |
| 18.01.2022 | David | Eratosthen | [lec17](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.01.18/prezentace17.pdf) |  |
| 25.01.2022 | David | Dictionary | [lec18](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.01.25/prezentace18.pdf) |  |
| 01.02.2022 | David | Aritmetika | [lec20](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.02.01/prezentace20.pdf) | [hw07](https://gitlab.com/hajekdd/septimy-21-22/-/tree/main/homework/hw07) |
| 08.02.2022 | David | Merge sort | [lec21](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.02.08/prezentace21.pdf) | |
| 15.02.2022 | David | Opakování + gcd a lcm | [lec22](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.02.15/prezentace19.pdf) | |
| 22.02.2022 | David | winform | [lec23](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.02.22/prezentace22.pdf) |[hw10](https://gitlab.com/hajekdd/septimy-21-22/-/tree/main/homework/hw10) |
| 01.03.2022 | David | Samostatná práce - morzeovka | [zadání](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.03.01/) [šablona](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.03.01/MorseTranslator.cs) |
| 08.03.2022 | David | Grafika | | |
| 15.03.2022 | David | Jarňáky | | |
| 22.03.2022 | David | Testíček | [zadání](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.03.22/) | |
| 29.03.2022 | David | Testíček - C | [zadání - C](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.03.29/Test) [práce - fileApp](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.03.29/Prace)  | |
| 05.04.2022 | David | Procvičování | [práce 1](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.04.05/Prace) <br /> [práce 2 - závorky](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.04.05/Prace2)  | |
| 12.04.2022 | David | GIT | [práce](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.04.12/Prace) <br /> [git](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.04.12/)  | |
| 19.04.2022 | David | | | |
| 26.04.2022 | David | OOP opakování | [OOP opakovani](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.04.26/)  | |
| 31.05.2022 | David | Opakování - integrálek | [OOP opakovani](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2022.05.31/)  | |
